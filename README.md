# Deep Learning 101
Thanks for checking out.
This repository contains my personal notes, codes and practice assignments implementing basics of machine learning and deep learning. Stay hyped !

### Contents
| # | Assignment Topic |
| ------ | ----------------- |
| 1 | [Determine animal's body weight given its brain weight][expt1] |
| 2 | [Relationship between student test scores and study hours][expt2] |
-----
Happily hosted on Staticaly and GitLab

[expt1]: <https://cdn.staticaly.com/gl/amitrajitbose/deepLearning101/raw/master/linearRegression.html>
[expt2]: <https://cdn.staticaly.com/gl/amitrajitbose/deepLearning101/raw/master/linearRegressionUsingGradientDescent.html>